package task01;

import lombok.Value;

@Value
public class Quadrat implements Geometry{
    private double side;

    @Override
    public double square() {
        double square = side*side;
        return square;
    }
}
