package task01;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double sumSquare = sum(
                new Circle(5),
                new Triangle(8, 6),
                new Quadrat(12));
        System.out.println(sumSquare);
    }
    static double sum(Geometry... geometry) {
        double sum = 0;
        for (int i = 0; i < geometry.length; i++) {
            sum += geometry[i].square();
        }
        return sum;
    }
}
