package task01;

import lombok.Value;

@Value
public class Triangle implements Geometry{
    private double height;
    private double lenght;

    @Override
    public double square() {
        double square = (height*lenght)/2;
        return square;
    }
}
