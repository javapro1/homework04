package task01;

import lombok.Value;

import java.util.Scanner;

@Value
public class Circle implements Geometry {
    private final double PI = 3.14;
    private double radius;
    //   Scanner input = new Scanner(System.in);

//    public Circle(double radius) {
//        this.radius = radius;
//    }

    @Override
    public double square() {
        double square = PI * Math.pow(radius, 2);
        return square;
    }
}
