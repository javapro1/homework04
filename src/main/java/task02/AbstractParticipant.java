package task02;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public abstract class AbstractParticipant implements Participant{
    double limitRunning;
    double limitJumping;
    String name;
}
