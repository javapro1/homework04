package task02;

import lombok.Value;

@Value
public class Robot extends AbstractParticipant implements Participant{
    public Robot(double limitRunning, double limitJumping, String name) {
        super(limitRunning, limitJumping, name);
    }

    @Override
    public void jump() {
        System.out.println("Robot is jumping");
    }

    @Override
    public void run() {
        System.out.println("Robot is running");
    }
}
