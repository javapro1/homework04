package task02;

public class Cat extends AbstractParticipant {

    public Cat(double limitRunning, double limitJumping, String name) {
        super(limitRunning, limitJumping, name);
    }

    @Override
    public void jump() {
        System.out.println("Cat is jumping");
    }

    @Override
    public void run() {
        System.out.println("Cat is running");
    }

    @Override
    public String getName() {
        return name;
    }
}
