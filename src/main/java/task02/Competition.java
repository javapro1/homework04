package task02;

public class Competition {
    public void startCompetition(Participant[] sportsman, Obstacle[] obstacles){
        for(int i=0; i<sportsman.length; i++){
            for (int j=0; j<obstacles.length; j++){
                if (!obstacles[j].overcome(sportsman[i])){
                    break;
                };
            }
        }
    }
}
