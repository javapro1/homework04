package task02;

public interface Participant {

     void jump();
     void run();
     double getLimitRunning();
     double getLimitJumping();
     String getName();
}
