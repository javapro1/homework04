package task02;

import lombok.Value;

@Value
public class RunningTrack implements Obstacle {
    double length;

    public RunningTrack(double length) {
        this.length = length;
    }

    @Override
    public boolean overcome(Participant participant) {
        if (participant.getLimitRunning() > length) {
            participant.run();
            System.out.println("Participant " + participant.getName() + " finished the RUNNING TRACK with length " + getLength() + " meters");
            return true;
        } else {
            System.out.println("Participant " + participant.getName() + " did not finish the RUNNING TRACK with length " + getLength() + " meters");
            return false;
        }
    }
}
