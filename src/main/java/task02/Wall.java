package task02;

import lombok.Value;

@Value
public class Wall implements Obstacle {
    double height;

    public double getHeight() {
        return height;
    }

    @Override
    public boolean overcome(Participant participant) {
        if (participant.getLimitJumping() > height) {
            participant.jump();
            System.out.println("Participant " + participant.getName() + " finished the WALL with height " + getHeight() + " meters");
            return true;
        } else {
            System.out.println("Participant " + participant.getName() + " did not finish the WALL with height " + getHeight() + " meters");
            return false;
        }
    }
}
