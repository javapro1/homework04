package task02;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
//        Cat cat = new Cat(8, 15);
//        Robot robot = new Robot(20, 25);
//        Human human = new Human(5, 8);
//        RunningTrack track = new RunningTrack(10);
//        Wall wall = new Wall(5);
        Scanner input = new Scanner(System.in);
        Participant[] sportsman = new Participant[]
                {
                        new Cat(5, 3,"cat"),
                        new Robot(4, 8, "robot"),
                        new Human(12, 9, "human")
                };
        Obstacle[] obstacles = new Obstacle[]{
                new RunningTrack(6),
                new Wall(8)};
        Competition competition = new Competition();
        competition.startCompetition(sportsman, obstacles);
    }


}
