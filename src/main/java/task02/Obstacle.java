package task02;

public interface Obstacle {
     boolean overcome(Participant participant);
}
