package task02;

public class Human extends  AbstractParticipant implements Participant{
    public Human(double limitRunning, double limitJumping, String name) {
        super(limitRunning, limitJumping, name);
    }

    @Override
    public void jump() {
        System.out.println("Human is jumping");
    }

    @Override
    public void run() {
        System.out.println("Human is running");
    }
}
